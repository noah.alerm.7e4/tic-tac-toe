import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import models.Board
import models.Position
import java.io.OutputStream
import java.net.ServerSocket
import java.net.Socket
import java.nio.charset.Charset
import java.util.*
import kotlin.concurrent.thread

private var isUsing:Boolean = false

//METHODS
/**
 * This method is used to move a piece to a given position.
 */
private fun moveTo(pos: Position, player: Boolean){
    // MOVE ROBOT
    println("${pos.x} ${pos.y} $player")
}

/**
 * This method is used to check if the game is over.
 */
private fun isGameOver(board: Board) : Boolean {
    var isOver = false

    //HORIZONTAL
    for (column in board.board) {
        val firstCell = column.first()

        if (firstCell != null &&
            ((firstCell == column[column.indexOf(firstCell)+1] && firstCell == column[column.indexOf(firstCell)+2])))
            isOver = true
    }

    //VERTICAL
    for (i in 0..2) {
        if (board.board[0][i] != null && (board.board[0][i] == board.board[1][i] && board.board[0][i] == board.board[2][i]))
            isOver = true
    }

    //DIAGONAL
    if ((board.board[0][0] != null && (board.board[0][0] == board.board[1][1] && board.board[0][0] == board.board[2][2]))
        || (board.board[2][0] != null && (board.board[2][0] == board.board[1][1] && board.board[2][0] == board.board[0][2])) )
        isOver = true

    return isOver
}

/**
 * This method is used to check if there's a draw.
 */
private fun isDraw(board: Board) : Boolean {
    var isDraw = true

    for (column in board.board) {
        if (column.contains(null)) {
            isDraw = false
            break
        }
    }

    return isDraw
}

/**
 * This method is used to calculate the robot's next movement.
 */
private fun getNewPos(board: Board) : Position {
    //println(board.getEmptyCells())
    return board.getEmptyCells().random()
}

//APP
@Composable
@Preview
fun app() {
    var board by remember { mutableStateOf(Board()) }
    val checkedState = remember { mutableStateOf(false) }

    MaterialTheme {
        Switch(
            checked = checkedState.value,
            onCheckedChange = { checkedState.value = it },
            modifier = Modifier.padding(60.dp, 0.dp)
        )
        Text(
            "PvP",
            textAlign = TextAlign.Center,
            fontSize = 25.sp,
            modifier = Modifier.padding(10.dp, 10.dp)
        )
        Text(
            "PvR",
            textAlign = TextAlign.Center,
            fontSize = 25.sp,
            modifier = Modifier.padding(115.dp, 10.dp)
        )

        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            board.board.forEachIndexed { y, row ->
                Row {
                    row.forEachIndexed { x, cell ->
                        val text = when(cell){
                            true -> "X"
                            false -> "O"
                            else -> " "
                        }

                        Box(modifier = Modifier.width(80.dp).height(80.dp).border(1.dp, Color.Black).clickable {
                            //MOVEMENT
                            if (!isGameOver(board) && !isUsing && cell == null) {
                                //PvP or PvR (Player)
                                if (!checkedState.value || (checkedState.value && board.currentPlayer)) {
                                    board.playAt(Position(x, y))
                                    val aux = board
                                    board = Board()
                                    board = aux
                                    move(Position(x, y))
                                }
                            }
                        }) {
                            //PvR (Robot)
                            if (!isGameOver(board) && checkedState.value && !board.currentPlayer) {
                                val pos = getNewPos(board)

                                board.playAt(pos)
                                val aux = board
                                board = Board()
                                board = aux
                                move(pos)
                            }

                            Text(
                                text,
                                modifier = Modifier.align(Alignment.Center),
                                textAlign = TextAlign.Center,
                                fontSize = 25.sp,
                            )

//                            if (text == "O") {
//                                Image(
//                                    painter = painterResource("circle.png"),
//                                    contentDescription = "Sample",
//                                    modifier = Modifier.width(50.dp).height(50.dp).align(Alignment.Center)
//                                )
//                            }
                        }
                    }
                }
            }

            //NEW GAME BUTTON
            Button(modifier = Modifier.align(Alignment.CenterHorizontally).padding(50.dp),
                onClick = {
                    board = Board()
                },
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Black, contentColor = Color.White)
            ) {
                Text("New game")
            }
        }

        //GAME OVER
        if (isGameOver(board) || isDraw(board)) {
            println("GAME OVER")

            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Bottom
            ) {
                Text(
                    "GAME OVER",
                    textAlign = TextAlign.Center,
                    fontSize = 30.sp
                )

                if (!isDraw(board)) {
                    val winner: String = if (!board.currentPlayer)
                        "X"
                    else
                        "O"

                    //WINNER
                    Text(
                        "WINNER: $winner",
                        modifier = Modifier.padding(20.dp),
                        textAlign = TextAlign.Center,
                        fontSize = 20.sp
                    )
                }
                else {
                    //DRAW
                    Text(
                        "DRAW",
                        modifier = Modifier.padding(20.dp),
                        textAlign = TextAlign.Center,
                        fontSize = 20.sp
                    )
                }
            }
        }
    }
}

//MAIN
fun main() = application {
    Window(onCloseRequest = ::exitApplication, title = "Tic-Tac-Toe") {
        app()
    }
}

fun move(pos: Position) {
    val server = ServerSocket(50000)
    println("Server is running on port ${server.localPort}")
    isUsing = true

    val client = server.accept()
    println("Client connected: ${client.inetAddress.hostAddress}")

    // Run client in it's own thread.
    thread { ClientHandler(client, pos, server).run() }
}
class ClientHandler(private val client: Socket, private val pos: Position, private val server: ServerSocket) {
    private val reader: Scanner = Scanner(client.getInputStream())
    private val writer: OutputStream = client.getOutputStream()
    private var running: Boolean = false

    fun run() {
        running = true
        // Welcome message
        write("Welcome to the server! To Exit, write: 'EXIT'.\n")

        while (running) {
            try {
                write(getCellNum(pos))

                val text = reader.nextLine()
                if (text == "EXIT"){
                    shutdown()
                    continue
                }
            } catch (ex: Exception) {
                shutdown()
            }
        }
    }
    private fun write(message: String) {
        writer.write((message + '\n').toByteArray(Charset.defaultCharset()))
    }
    private fun shutdown() {
        running = false
        isUsing = false
        client.close()
        server.close()
        println("${client.inetAddress.hostAddress} closed the connection")
    }

    private fun getCellNum(pos: Position) : String {
        var cell = ""

        when (pos) {
            Position(0, 0) -> cell = "[0,0]"
            Position(0, 1) -> cell = "[0,1]"
            Position(0, 2) -> cell = "[0,2]"
            Position(1, 0) -> cell = "[1,0]"
            Position(1, 1) -> cell = "[1,1]"
            Position(1, 2) -> cell = "[1,2]"
            Position(2, 0) -> cell = "[2,0]"
            Position(2, 1) -> cell = "[2,1]"
            Position(2, 2) -> cell = "[2,2]"
        }

        return cell
    }
}
