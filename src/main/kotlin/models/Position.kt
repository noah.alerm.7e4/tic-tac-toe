package models

data class Position(val x: Int, val y: Int)
