package models

data class Board(val board: List<MutableList<Boolean?>> = List(3){MutableList(3){null}}, var currentPlayer: Boolean = true) {
    //ATTRIBUTES
    private var emptyCells = mutableListOf<Position>()

    init {
        emptyCells = mutableListOf(
            Position(0,0),
            Position(0,1),
            Position(0,2),
            Position(1,0),
            Position(1,1),
            Position(1,2),
            Position(2,0),
            Position(2,1),
            Position(2,2)
        )
    }

    //METHODS
    /**
     * This method is used to set a new piece on the given position.
     */
    fun playAt(pos: Position){
        if (board[pos.y][pos.x] == null) {
            board[pos.y][pos.x] = currentPlayer

            //EMPTY CELLS UPDATE
            emptyCells.remove(pos)

            //PLAYER CHANGE
            currentPlayer = !currentPlayer
        }
    }

    /**
     * This method is used to obtain all the empty cells.
     */
    fun getEmptyCells() : List<Position> {
        return emptyCells
    }
}
